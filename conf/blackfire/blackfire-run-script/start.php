<?php

function isBlackfireProfiling()
{
    $isIndex = isset($_SERVER['SCRIPT_NAME']) && $_SERVER['SCRIPT_NAME'] === '/index.php';
    return $isIndex;
}

try {
    require __DIR__ . '/vendor/autoload.php';
    $blackfireConfigFile = __DIR__ . '/blackfire.ini';
    if (file_exists($blackfireConfigFile) && isBlackfireProfiling()) {
        try {
            $config = \Blackfire\ClientConfiguration::createFromFile($blackfireConfigFile);

            $blackfire = new \Blackfire\Client($config);
            $probe = $blackfire->createProbe();

            register_shutdown_function(function () use ($blackfire, $probe) {
                $profile = $blackfire->endProbe($probe);
            });
        } catch (\Blackfire\Exception\ExceptionInterface $e) {

        }
    }
} catch (\Exception $e) {

}
