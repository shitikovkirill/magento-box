require 'yaml'
vagrantConfig = YAML.load_file 'Vagrantfile.config.yml'

$system = <<-SHELL
        sudo add-apt-repository ppa:ondrej/php -y
        sudo apt-get -y update
  	SHELL

$php = <<-SHELL
        echo -e "\e[34mInstall PHP"
		sudo apt-get -y install php7.2 php7.2-common php7.2-gd php7.2-curl php7.2-intl php7.2-xsl php7.2-mbstring php7.2-zip php7.2-iconv php7.2-bcmath php7.2-mysql php7.2-soap
        php -v

        echo -e "\e[34mAdd magento fpm conf file"
        cat '/vagrant/conf/fpm/magento.conf' | sudo tee /etc/php/7.2/fpm/pool.d/magento.conf

        sudo apt-get -y install unzip phpunit

        echo -e "\e[34mAdd phpinfo"
        sudo mkdir -p /var/www/html/info.lock/public_html/
        cat '/vagrant/conf/php/phpinfo.php' | sudo tee /var/www/html/info.lock/public_html/phpinfo.php
  	SHELL

$php_xdebug = <<-SHELL
        echo -e "\e[34mInstall XDebug"
        sudo apt-get install -y php-xdebug
        cat '/vagrant/conf/xdebug/20-xdebug.ini' | sudo tee ls /etc/php/7.2/apache2/conf.d/20-xdebug.ini
        systemctl restart apache2
  	SHELL

$fpm = <<-SHELL
        echo -e "\e[34mInstall PHP-FPM"
		sudo apt-get -y install php7.2 php7.2-fpm
		php -v

        echo -e "\e[34mFix fpm ini file"
        sudo sed -i "s/memory_limit = .*/memory_limit = 1024M/"                    /etc/php/7.2/fpm/php.ini
        sudo sed -i "s/upload_max_filesize = .*/upload_max_filesize = 256M/"       /etc/php/7.2/fpm/php.ini
        sudo sed -i "s/zlib.output_compression = .*/zlib.output_compression = on/" /etc/php/7.2/fpm/php.ini
        sudo sed -i "s/max_execution_time = .*/max_execution_time = 18000/"        /etc/php/7.2/fpm/php.ini
        sudo sed -i "s/;date.timezone.*/date.timezone = UTC/"                      /etc/php/7.2/fpm/php.ini
        sudo sed -i "s/;opcache.save_comments.*/opcache.save_comments = 1/"        /etc/php/7.2/fpm/php.ini
    SHELL

$composer = <<-SHELL
        echo -e "\e[34mInstall composer"
        mkdir -p /tmp/composer
        cd /tmp/composer
        curl -sS https://getcomposer.org/installer | php
        mv composer.phar /usr/local/bin/composer

        echo -e "\e[34mAuthenticate composer"
        mkdir -p /home/vagrant/.composer
        cat '/vagrant/conf/composer/auth.json' | tee /home/vagrant/.composer/auth.json

        sed -i "s/public_magento_key/#{vagrantConfig['http_basic']['repo_magento_com']['username']}/"  /home/vagrant/.composer/auth.json
        sed -i "s/private_magento_key/#{vagrantConfig['http_basic']['repo_magento_com']['password']}/" /home/vagrant/.composer/auth.json
        sed -i "s/github_key/#{vagrantConfig['github_oauth']['github_com']}/"                          /home/vagrant/.composer/auth.json
    SHELL

$apache = <<-SHELL
        echo -e "\e[34mInstall apache"
        sudo apt-get -y install apache2 libapache2-mod-php7.2
        apache2 -v

        echo -e "\e[34mFix apache2 ini file"
        sudo sed -i "s/memory_limit = .*/memory_limit = 1024M/"                    /etc/php/7.2/apache2/php.ini
        sudo sed -i "s/upload_max_filesize = .*/upload_max_filesize = 256M/"       /etc/php/7.2/apache2/php.ini
        sudo sed -i "s/zlib.output_compression = .*/zlib.output_compression = on/" /etc/php/7.2/apache2/php.ini
        sudo sed -i "s/max_execution_time = .*/max_execution_time = 18000/"        /etc/php/7.2/apache2/php.ini
        sudo sed -i "s/;date.timezone.*/date.timezone = UTC/"                      /etc/php/7.2/apache2/php.ini
        sudo sed -i "s/;opcache.save_comments.*/opcache.save_comments = 1/"        /etc/php/7.2/apache2/php.ini
        
        sudo a2enmod rewrite
        cat '/vagrant/conf/apache2/info.lock.conf' | sudo tee /etc/apache2/sites-available/info.lock.conf
        cat '/vagrant/conf/apache2/magento.lock.conf' | sudo tee /etc/apache2/sites-available/magento.lock.conf

        sudo a2ensite info.lock
        sudo a2ensite magento.lock

        echo "User vagrant" >> /etc/apache2/apache2.conf
        echo "Group vagrant" >> /etc/apache2/apache2.conf

        sudo systemctl restart apache2
    SHELL

$mysql = <<-SHELL
        echo -e "\e[34mInstall mysql"
        sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password #{vagrantConfig['mysql']['password']}'
        sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password #{vagrantConfig['mysql']['password']}'
        sudo apt-get -y install mysql-server
        sudo update-rc.d mysql defaults

        sudo mysql --user=#{vagrantConfig['mysql']['username']} --password=#{vagrantConfig['mysql']['password']} -e "CREATE DATABASE IF NOT EXISTS #{vagrantConfig['magento']['db_name']} ;"
    SHELL

$magento = <<-SHELL
        echo -e "\e[34mInstall magento"

        mkdir -p /vagrant/app
        sudo ln -s /vagrant/app
        sudo ln -s /vagrant/app /var/www/html/magento.lock
        cd /vagrant/app

        composer create-project --repository-url=https://repo.magento.com/ magento/project-community-edition .
    SHELL

$magento_scripts = <<-SHELL
        echo -e "\e[34mMagento scripts"
        cd /vagrant/app

        php bin/magento setup:install --base-url="#{vagrantConfig['magento']['base_url']}" \
        --db-host="#{vagrantConfig['mysql']['host']}" \
        --db-user="#{vagrantConfig['mysql']['username']}" \
        --db-password="#{vagrantConfig['mysql']['password']}" \
        --db-name="#{vagrantConfig['magento']['db_name']}" \
        --admin-firstname="#{vagrantConfig['magento']['admin_firstname']}" \
        --admin-lastname="#{vagrantConfig['magento']['admin_lastname']}" \
        --admin-email="#{vagrantConfig['magento']['admin_email']}" \
        --admin-user="#{vagrantConfig['magento']['admin_user']}" \
        --admin-password="#{vagrantConfig['magento']['admin_password']}" \
        --backend-frontname="#{vagrantConfig['magento']['backend_frontname']}" \
        --language="#{vagrantConfig['magento']['language']}" \
        --currency="#{vagrantConfig['magento']['currency']}" \
        --timezone="#{vagrantConfig['magento']['timezone']}"

        php bin/magento deploy:mode:set developer
        # php bin/magento cache:disable
        # php bin/magento cache:flush
        php bin/magento setup:performance:generate-fixture setup/performance-toolkit/profiles/ce/small.xml
    SHELL

$magento_permissions = <<-SHELL
        echo -e "\e[34mPermissions"
        cd /vagrant/app

        find . -print -type f -exec chmod 644 {} \;                              # 644 permission for files
        find . -print -type d -exec chmod 755 {} \;                              # 755 permission for directory
        find ./var ./pub/media ./pub/static -print -type d -exec chmod 777 {} \; # 777 permission for var folder
        chmod 777 ./app/etc
        chmod 644 ./app/etc/*.xml
        chmod u+x bin/magento
    SHELL

$blackfire = <<-SHELL
        echo -e "\e[34mConfiguring the Debian Repository"
        wget -q -O - https://packages.blackfire.io/gpg.key | sudo apt-key add -
        echo "deb http://packages.blackfire.io/debian any main" | sudo tee /etc/apt/sources.list.d/blackfire.list

        echo -e "\e[34mInstalling the Agent"
        sudo apt-get update
        sudo apt-get install -y blackfire-agent

        cat '/vagrant/conf/blackfire/agent' | sudo tee /etc/blackfire/agent

        sudo sed -i "s/server-id=server_id/server-id=#{vagrantConfig['blackfire']['server_id']}/"             /etc/blackfire/agent
        sudo sed -i "s/server-token=server_token/server-token=#{vagrantConfig['blackfire']['server_token']}/" /etc/blackfire/agent

        sudo /etc/init.d/blackfire-agent start

        echo -e "\e[34mInstalling the PHP Probe"
        sudo apt-get install -y blackfire-php
    SHELL

$blackfire_run_script = <<-SHELL
        echo -e "\e[34mBlackfire run script"
        cp -R /vagrant/conf/blackfire/blackfire-run-script/ /usr/lib/
        cd /usr/lib/blackfire-run-script/
        composer install

        cat 'blackfire.ini.sample' | sudo tee blackfire.ini

        echo -e "\e[34mSet client-id"
        sed -i "s/client-id=.*/client-id=#{vagrantConfig['blackfire']['client_id']}/"          blackfire.ini

        echo -e "\e[34mSet client-token"
        sed -i "s/client-token=.*/client-token=#{vagrantConfig['blackfire']['client_token']}/" blackfire.ini

        echo -e "\e[34mSet auto_prepend_file"
        cat /etc/php/7.2/apache2/php.ini | grep auto_prepend_file
        sed -i "s~auto_prepend_file =.*~auto_prepend_file = /usr/lib/blackfire-run-script/start.php~" /etc/php/7.2/apache2/php.ini
    SHELL

$tideways = <<-SHELL
        echo 'deb http://s3-eu-west-1.amazonaws.com/tideways/packages debian main' > /etc/apt/sources.list.d/tideways.list
        wget -qO - https://s3-eu-west-1.amazonaws.com/tideways/packages/EEB5E8F4.gpg | sudo apt-key add -
        apt-get update
        apt-get install -y tideways-php tideways-daemon

        cat '/vagrant/conf/tideways/tideways.ini' | sudo tee /etc/php/7.2/apache2/conf.d/40-tideways.ini
        sed -i "s/tideways.api_key=.*/tideways.api_key=#{vagrantConfig['tideways']['api_key']}/" /etc/php/7.2/apache2/conf.d/40-tideways.ini
        systemctl restart apache2
    SHELL

Vagrant.configure(2) do |config|
    config.vm.box = "ubuntu/xenial64"
    config.vm.network "private_network", ip: vagrantConfig['ip']

    # VirtualBox specific settings
    config.vm.provider "virtualbox" do |vb|
        vb.gui = false
        vb.memory = "4096"
        vb.cpus = 4
    end

    # System
    config.vm.provision "shell", inline: $system, name: "system"

    # PHP
    config.vm.provision "shell", inline: $php, name: "php"

    # Composer
    config.vm.provision "shell", inline: $composer, name: "composer"

    # Apache
    config.vm.provision "shell", inline: $apache, name: "apache"

    # XDebug
    config.vm.provision "shell", inline: $php_xdebug, name: "$XDebug"

    # MySql
    config.vm.provision "shell", inline: $mysql, name: "mysql"

    # Magento
    config.vm.provision "shell", inline: $magento, name: "magento", privileged: false

    # Magento permissions
    # config.vm.provision "shell", inline: $magento_permissions, name: "magento permissions"

    # Magento install
    config.vm.provision "shell", inline: $magento_scripts, name: "magento scripts", privileged: true

    # Installing Blackfire
    config.vm.provision "shell", inline: $blackfire, name: "Installing Blackfire", privileged: false

    # Blackfire run script
    # config.vm.provision "shell", inline: $blackfire_run_script, name: "Blackfire run script", privileged: true

    # Install tideways
    config.vm.provision "shell", inline: $tideways, name: "Install tideways", privileged: true
    
end

